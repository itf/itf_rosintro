#!/usr/bin/env python
# Code derived from move_group_python_interface_tutorial.py by:
# Author: Acorn Pooley, Mike Lautman
## Author: Ian Flynn

# Python 2/3 compatibility imports
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
# Import below used in conjunction with code from : http://wiki.ros.org/tf2/Tutorials/Quaternions
from tf.transformations import quaternion_from_euler

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

## END_SUB_TUTORIAL


def all_close(goal, actual, tolerance):
    """
    Convenience method for testing if the values in two lists are within a tolerance of each other.
    For Pose and PoseStamped inputs, the angle between the two quaternions is compared (the angle
    between the identical orientations q and -q is calculated correctly).
    @param: goal       A list of floats, a Pose or a PoseStamped
    @param: actual     A list of floats, a Pose or a PoseStamped
    @param: tolerance  A float
    @returns: bool
    """
    if type(goal) is list:
        for index in range(len(goal)):
            if abs(actual[index] - goal[index]) > tolerance:
                return False

    elif type(goal) is geometry_msgs.msg.PoseStamped:
        return all_close(goal.pose, actual.pose, tolerance)

    elif type(goal) is geometry_msgs.msg.Pose:
        x0, y0, z0, qx0, qy0, qz0, qw0 = pose_to_list(actual)
        x1, y1, z1, qx1, qy1, qz1, qw1 = pose_to_list(goal)
        # Euclidean distance
        d = dist((x1, y1, z1), (x0, y0, z0))
        # phi = angle between orientations
        cos_phi_half = fabs(qx0 * qx1 + qy0 * qy1 + qz0 * qz1 + qw0 * qw1)
        return d <= tolerance and cos_phi_half >= cos(tolerance / 2.0)

    return True


class MoveGroupPythonInterfaceTutorial(object):
    """MoveGroupPythonInterfaceTutorial"""

    def __init__(self):
        super(MoveGroupPythonInterfaceTutorial, self).__init__()

        ## BEGIN_SUB_TUTORIAL setup
        ##
        ## First initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

        ## Instantiate a `RobotCommander`_ object. Provides information such as the robot's
        ## kinematic model and the robot's current joint states
        robot = moveit_commander.RobotCommander()

        ## Instantiate a `PlanningSceneInterface`_ object.  This provides a remote interface
        ## for getting, setting, and updating the robot's internal understanding of the
        ## surrounding world:
        scene = moveit_commander.PlanningSceneInterface()

        ## Instantiate a `MoveGroupCommander`_ object.  This object is an interface
        ## to a planning group (group of joints).  In this tutorial the group is the primary
        ## arm joints in the Panda robot, so we set the group's name to "panda_arm".
        ## If you are using a different robot, change this value to the name of your robot
        ## arm planning group.
        ## This interface can be used to plan and execute motions:
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        ## Create a `DisplayTrajectory`_ ROS publisher which is used to display
        ## trajectories in Rviz:
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        ## END_SUB_TUTORIAL

        ## BEGIN_SUB_TUTORIAL basic_info
        ##
        ## Getting Basic Information
        ## ^^^^^^^^^^^^^^^^^^^^^^^^^
        # We can get the name of the reference frame for this robot:
        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        # We can also print the name of the end-effector link for this group:
        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        # We can get a list of all the groups in the robot:
        group_names = robot.get_group_names()
        print("============ Available Planning Groups:", robot.get_group_names())

        # Sometimes for debugging it is useful to print the entire state of the
        # robot:
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")
        ## END_SUB_TUTORIAL

        # Misc variables
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

    def go_to_joint_state(self):
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.
        move_group = self.move_group

        # Joint angles below set the ur5e into a standing position and away from the singularity
        # found at the starting position
        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = tau / 8 # 1/8 turn
        joint_goal[1] = -tau / 6 # -1/6 turn
        joint_goal[2] = tau / 8 # 1/8 turn
        joint_goal[3] = -tau / 5 # -1/5 turn
        joint_goal[4] = -tau / 4 # -1/4 turn
        joint_goal[5] = tau / 8 # 1/8 turn, done to operate within tolerance limits

        # The go command can be called with joint values, poses, or without any
        # parameters if you have already set the pose or joint target for the group
        move_group.go(joint_goal, wait=True)

        # Calling ``stop()`` ensures that there is no residual movement
        move_group.stop()

        ## END_SUB_TUTORIAL

        # For testing:
        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)

    def go_to_pose_goal(self, x_position, y_position, z_position):
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.
        move_group = self.move_group

        ## BEGIN_SUB_TUTORIAL plan_to_pose
        ##
        ## Planning to a Pose Goal
        ## ^^^^^^^^^^^^^^^^^^^^^^^
        ## We can plan a motion for this group to a desired pose for the
        ## end-effector:

        current_orientation = move_group.get_current_pose().pose.orientation
        
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.position.x = x_position
        pose_goal.position.y = y_position
        pose_goal.position.z = z_position
        
        # Set the x quaternion to 1, while the rest are set to zero to maintain the end effector pointing downward
        pose_goal.orientation.x = 1
        pose_goal.orientation.y = 0
        pose_goal.orientation.z = 0
        pose_goal.orientation.w = 0

        move_group.set_pose_target(pose_goal)

        ## Now, we call the planner to compute the plan and execute it.
        # `go()` returns a boolean indicating whether the planning and execution was successful.
        success = move_group.go(wait=True)
        # Calling `stop()` ensures that there is no residual movement
        move_group.stop()
        # It is always good to clear your targets after planning with poses.
        # Note: there is no equivalent function for clear_joint_value_targets().
        move_group.clear_pose_targets()

        ## END_SUB_TUTORIAL

        # For testing:
        # Note that since this section of code will not be included in the tutorials
        # we use the class variable rather than the copied state variable
        current_pose = self.move_group.get_current_pose().pose
        return all_close(pose_goal, current_pose, 0.01)

    

        ## END_SUB_TUTORIAL

    # Functions made for midterm

    # This command draws a horizontal bar, with positive lengths indicating a positive direction (+global x in this case), and negative the opposite
    def draw_horizontal(self, length):

        move_group = self.move_group

        waypoints = []

        wpose = move_group.get_current_pose().pose

        wpose.position.x += length * 0.1
        waypoints.append(copy.deepcopy(wpose))

        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        # Execute the horizontal drawing
        self.execute_plan(plan)

    
    # This command draws a vertical bar, with positive lengths indicating a positive direction (+global y in this case), and negative the opposite
    def draw_vertical(self, length):

        move_group = self.move_group

        waypoints = []

        wpose = move_group.get_current_pose().pose

        wpose.position.y += length * 0.1
        waypoints.append(copy.deepcopy(wpose))

        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        # Execute the vertical drawing
        self.execute_plan(plan)

    # This command returns to the starting position for drawing
    # Made its own function for readability
    def return_to_start(self):
        self.go_to_pose_goal(0.2, 0.2, 0.4)

    # This command draws the 'I' with the given size representing the total height, while also maintaining given proportions
    def draw_I(self, size):
        move_group = self.move_group
        # Go to starting position
        self.return_to_start()
        
        # Draw the bottom horizontal bar
        self.draw_horizontal(size / 2)
        self.draw_horizontal(-size)

        # Return to the center
        self.return_to_start()

        # Draw the vertical bar
        self.draw_vertical(size)
        
        # Draw the top horizontal bar
        self.draw_horizontal(size / 2)
        self.draw_horizontal(-size)

        # Return to the start
        self.return_to_start()

    # This command draws the 'T' with the given size representing the total height, while also maintaining given proportions
    def draw_T(self, size):
        # Draw the vertical bar
        self.draw_vertical(size)

        # Draw the top horizontal bar
        self.draw_horizontal(size / 2)
        self.draw_horizontal(-size)

        # Return to the start
        self.return_to_start()


    # This command draws the 'F' with the given size representing the total height, while also maintaining given proportions
    def draw_F(self, size):
        self.return_to_start()

        # Draw the vertical bar up until the point to add the first horizontal bar of the capital 'F'
        self.draw_vertical(size * 0.75)

        # Add the first bar of the 'F'
        self.draw_horizontal(size / 2)
        self.draw_horizontal(-size / 2)

        # Draw the rest of the vertical bar
        self.draw_vertical(size * 0.25)

        # Draw the top bar
        self.draw_horizontal(size / 2)

        # Return to the start
        self.return_to_start()
         
    
    # End of functions made for midterm

    def execute_plan(self, plan):
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.
        move_group = self.move_group

        ## BEGIN_SUB_TUTORIAL execute_plan
        ##
        ## Executing a Plan
        ## ^^^^^^^^^^^^^^^^
        ## Use execute if you would like the robot to follow
        ## the plan that has already been computed:
        move_group.execute(plan, wait=True)

        ## **Note:** The robot's current joint state must be within some tolerance of the
        ## first waypoint in the `RobotTrajectory`_ or ``execute()`` will fail
        ## END_SUB_TUTORIAL


def main():
    try:
        print("This is the demonstration of drawing 'ITF' for the midterm.")

        tutorial = MoveGroupPythonInterfaceTutorial()

        input("============ Press `Enter` to move to initial position.")

        tutorial.go_to_joint_state()

        input("============ Press `Enter` to execute the 'I'.")
        
        tutorial.draw_I(4)

        input("============ Press `Enter` to execute the 'T'.")

        tutorial.draw_T(4)

        input("============ Press `Enter` to execute the 'F'.")

        tutorial.draw_F(4)


        print("============ Drawing complete!")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()