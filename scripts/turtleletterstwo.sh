rosservice call /spawn 3 3 0.0 "I_turtle"
rosservice call /spawn 9 9 0.0 "a_turtle"
rosservice call /I_turtle/set_pen 255 86 69 2 'false'
rosservice call /a_turtle/set_pen 255 200 69 2 'false'
rostopic pub -1 /I_turtle/cmd_vel geometry_msgs/Twist \
    -- '[0.0, 2.5, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /I_turtle/cmd_vel geometry_msgs/Twist \
    -- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /I_turtle/cmd_vel geometry_msgs/Twist \
    -- '[-4.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /I_turtle/cmd_vel geometry_msgs/Twist \
    -- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /I_turtle/cmd_vel geometry_msgs/Twist \
    -- '[0.0, -5.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /I_turtle/cmd_vel geometry_msgs/Twist \
    -- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /I_turtle/cmd_vel geometry_msgs/Twist \
    -- '[-4.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /a_turtle/cmd_vel geometry_msgs/Twist \
    -- '[5.0, 0.0, 0.0]' '[0.0, 0.0, -8.0]'
rostopic pub -1 /a_turtle/cmd_vel geometry_msgs/Twist \
    -- '[1.0, 0.0, 0.0]' '[0.0, 0.0, 2.0]'